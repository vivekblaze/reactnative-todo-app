import React from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'

export default function TotdoItem({ item, pressHandler }) {
    return (
        <TouchableOpacity onPress={() => pressHandler(item.key)}>
            <Text style={styles.item}>{item.text}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    item: {
        padding: 16,
        marginTop: 16,
        borderColor: '#BBB',
        borderWidth: 1,
        borderStyle: 'dashed',
        borderRadius: 10
    }
})