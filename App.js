import React, {useState} from 'react';
import {FlatList, StyleSheet, Text, View, Alert, TouchableWithoutFeedback, Keyboard} from 'react-native';
import AddTodo from './components/AddTodo';
import Header from './components/Header';
import TotdoItem from './components/TotdoItem';

export default function App() {
  // use hooks for functional comp
  const [todos, setTodos] = useState([
    {
      text: 'buy coffee', key: '1',
    },
    {
      text: 'create an app', key: '2',
    },
    {
      text: 'play on the switch', key: '3',
    },
  ]);

  const pressHandler = (key) => {
    setTodos((prevTodos) => {
      return prevTodos.filter(todo => todo.key != key);
    })
  }

  const submitHandler = (text) => {
    if (text.length > 3) {
      setTodos((prevTodos) => {
        return [
          { text: text, key: Math.random().toString() },
          ...prevTodos
        ];
      });
    } else {
      //using alert msg
      Alert.alert('OOPS!', 'Todos must be over 3 characters long.', [
        {text: 'Understood', onPress: () => console.log('Alert Closed')}
      ])
    }
  }

  return (
    <TouchableWithoutFeedback onPress={() => {
      Keyboard.dismiss();
      console.log('Dismissed Keyboard'); //test log
    }}>
      <View style={styles.container}>
        <Header />
        <View style={styles.content}>
          <AddTodo submitHandler={submitHandler}/>
          <View style={styles.list}>
            <FlatList 
              data={todos}
              renderItem={({item}) => (
                <TotdoItem item={item} pressHandler={pressHandler} />
              )}
            />
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  content: {
    padding: 40,
  },
  list: {
    marginTop: 20,
  },
});
